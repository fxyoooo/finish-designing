import Vue from 'vue'
import VueRouter from 'vue-router'
/* eslint-disable */
// 创建路由组件
import Main from '../components/Main.vue'
import AdService from '../views/AdService.vue'
import CartService from '../views/CartService.vue'
import CheckoutService from '../views/CheckoutService.vue'
import CurrencyService from '../views/CurrencyService.vue'
import EmailService from '../views/EmailService.vue'
import Frontend from '../views/Frontend.vue'
import PaymentService from '../views/PaymentService.vue'
import ProductcatalogService from '../views/ProductcatalogService.vue'
import RecommendationService from '../views/RecommendationService.vue'
import RedisService from '../views/RedisService.vue'
import ShippingService from '../views/ShippingService.vue'
import Details from '../components/Details.vue'
import metric from '../views/metric.vue'
import metricDetails from '../views/Details/index.vue'
Vue.use(VueRouter)

// 将路由与组件进行映射
const routes = [
  // 主路由
  {
    path: '/',
    component: Main,
    redirect: '/adservice', // 重定向
    children: [
      // 子路由
      {path: 'adservice', component: AdService},
      {path: 'cartservice', component: CartService}, // 商品管理
      {path: 'checkoutservice', component: CheckoutService}, // 用户管理
      {path: 'currencyservice', component: CurrencyService}, // 页面一
      {path: 'emailservice', component: EmailService}, // 页面二
      {path: 'frontend', component: Frontend},
      {path: 'paymentservice', component: PaymentService},
      {path: 'productcatalogservice', component: ProductcatalogService},
      {path: 'recommendationservice', component: RecommendationService},
      {path: 'redis', component: RedisService},
      {path: 'shippingservice', component: ShippingService}
    ]
  },
  {
    path: '/details',
    component: Details,
    children: [
      {
        path: 'adservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      },
      {
        path: 'cartservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      }, // 商品管理
      {
        path: 'checkoutservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      }, // 用户管理
      {
        path: 'currencyservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      }, // 页面一
      {
        path: 'emailservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      }, // 页面二
      {
        path: 'frontend',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      },
      {
        path: 'paymentservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      },
      {
        path: 'productcatalogservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      },
      {
        path: 'recommendationservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      },
      {
        path: 'redis',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      },
      {
        path: 'shippingservice',
        component: Details,
        children: [
          {path: 'metric', component: metricDetails},
          {path: 'log'},
          {path: 'trace'}
        ]
      }
    ]
  }
]

// 创建router实例
const router = new VueRouter({
  routes // (缩写) 相当于 routes: routes
})

export default router
